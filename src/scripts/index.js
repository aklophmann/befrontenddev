import '../styles/index.scss';

// import $ from 'jquery';
// import 'bootstrap/dist/css/bootstrap.min.css';

$(document).ready(function () {

    var c, currentScrollTop = 0, navbar = $('nav');

   $(window).scroll(function () {
      let top = $(window).scrollTop();
      let navheight = navbar.height();
     
      currentScrollTop = top;
     
      if (c < currentScrollTop && top > navheight * 2) {
        navbar.addClass("scrollUp");
      } 
      else if (c > currentScrollTop && !(top <= navheight)) {
        navbar.removeClass("scrollUp");
      }
      c = currentScrollTop;
      if (c > 550){
        navbar.addClass("navBackground");
      }
      else{
        navbar.removeClass("navBackground");
      }
    });


    $('.acc-toggle').click(function(e){
        e.preventDefault();
        let items = document.getElementsByClassName('accordionItem');
        let target = document.getElementById(e.target.dataset.id);
        console.log(e.target.dataset.id);
        $(`#${e.target.dataset.id}`).toggle();

        items.forEach(element => {
            if (element != target){
                element.style.display = 'none';
            }
            
        });
    });

    $(function(){
        $('.bxslider').bxSlider({
          mode: 'fade',
          captions: true,
          slideWidth: 600
        });
    });
    
});

